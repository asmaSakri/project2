<?php

//load.php

$connect = new PDO('mysql:host=localhost;dbname=rse', 'root', '');

$data = array();
$numbr = 7;
$query = "SELECT * FROM events  ORDER BY id";

$statement = $connect->prepare($query);

$statement->execute();

$result = $statement->fetchAll();

foreach($result as $row)
{
 $data[] = array(
  'id'   => $row["id"],
  'title'   => $row["title"],
  'start'   => $row["start_event"],
  'end'   => $row["end_event"],
  'userid'   => $row["iduser"]
 );
}

echo json_encode($data);

?>
